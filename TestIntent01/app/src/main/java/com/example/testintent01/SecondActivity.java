package com.example.testintent01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent intent = getIntent();

        String text = intent.getStringExtra(MainActivity.KEY);
        TextView textView = findViewById(R.id.textView);
        //System.out.println(text);
        if(!text.equals("")) {
            textView.setText(text);
        }
        else {
            textView.setText("Saisie Vide");
        }
    }
}
