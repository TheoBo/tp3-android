package com.example.testintent01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    public final static String KEY = "text";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void send(View monBoutton){
        EditText editText = findViewById(R.id.editText);
        String text = editText.getText().toString();
        //System.out.println(text);

        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra(KEY, text);

        startActivity(intent);
    }
}
