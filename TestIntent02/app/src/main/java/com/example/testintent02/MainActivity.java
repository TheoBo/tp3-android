package com.example.testintent02;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public final static String KEY = "text";
    public final static int REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void modify(View monBoutton){
        EditText editText = findViewById(R.id.editText);
        String text = editText.getText().toString();
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra(KEY,text);

        startActivityForResult(intent, REQUEST_CODE);
    }

    public void validate(View monBoutton){
        TextView textView = findViewById(R.id.proposedModification);
        String modification = textView.getText().toString();
        if(!modification.equals("")){
            EditText editText = findViewById(R.id.editText);
            editText.setText(modification);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        String modifiedText = data.getStringExtra(SecondActivity.ANSWER_KEY);

        TextView textView = findViewById(R.id.proposedModification);
        textView.setText(modifiedText);
    }
}
