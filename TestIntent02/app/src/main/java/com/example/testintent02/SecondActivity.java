package com.example.testintent02;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    public final static String ANSWER_KEY = "answer";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent intent = getIntent();
        String text = intent.getStringExtra(MainActivity.KEY);

        TextView textView = findViewById(R.id.received);
        textView.setText(text);
    }

    /*public void validate(View monBoutton){
        EditText editText = findViewById(R.id.editText);
        String modifiedText = editText.getText().toString();
        sendAnswer(modifiedText)
    }*/

    public void uppercase(View monBoutton){
        TextView textView = findViewById(R.id.received);
        String modifiedText = textView.getText().toString();
        modifiedText = modifiedText.toUpperCase();
        sendAnswer(modifiedText);
    }

    public void invert(View monBoutton){
        TextView textView = findViewById(R.id.received);
        String modifiedText = textView.getText().toString();
        modifiedText = new StringBuilder(modifiedText).reverse().toString();
        sendAnswer(modifiedText);
    }

    private void sendAnswer(String modifiedText){
        Intent answer = new Intent();
        answer.putExtra(ANSWER_KEY, modifiedText);
        setResult(RESULT_OK, answer);
        finish();
    }
}
